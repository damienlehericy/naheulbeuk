#!/usr/bin/env python3
import hashlib
import model
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

engine = create_engine('sqlite:///naheul.db')
model.Base.metadata.create_all(engine)

session = Session(engine)
session.add_all([
    model.Race(id=1, name='Humain'),
    model.Race(id=2, name='Barbare'),
    model.Race(id=3, name='Nain'),
    model.Race(id=4, name='Haut-Elfe'),
    model.Race(id=5, name='Demi-Elfe'),
    model.Race(id=6, name='Elfe Sylvain'),
    model.Race(id=7, name='Elfe Noir'),
    model.Race(id=8, name='Orque'),
    model.Race(id=9, name='Demi-Orque'),
    model.Race(id=10, name='Gobelin'),
    model.Race(id=11, name='Ogre'),
    model.Race(id=12, name='Semi-Homme'),
    model.Race(id=13, name='Gnome des forets du nord'),
    model.Metier(id=1, name='Guerrier'),
    model.Metier(id=2, name='Ninja'),
    model.Metier(id=3, name='Voleur'),
    model.Metier(id=4, name='Pretre'),
    model.Metier(id=5, name='Mage'),
    model.Metier(id=6, name='Paladin'),
    model.Metier(id=7, name='Ranger'),
    model.Metier(id=8, name='Menestrel'),
    model.Metier(id=9, name='Pirate'),
    model.Metier(id=10, name='Marchand'),
    model.Metier(id=11, name='Ingenieur'),
    model.Metier(id=12, name='Bourgeois'),
])
session.commit()

