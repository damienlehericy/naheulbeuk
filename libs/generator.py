import random
from models import model
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

engine = create_engine('sqlite:///models/naheul.db')
engine.connect()
db = Session(engine)

def launch_dice(type):
    dice = 0
    if type == 6:
        dice = random.randint(1, 6)
    else:
        dice = random.randint(1, 20)

    return dice

def generate_monsters(name, room_id):
    base_stats = generate_base_stats()
    stats = generate_stats(base_stats)
    carac_data = model.Carac(
        COU=base_stats['COU'],
        INT=base_stats['INT'],
        CHA=base_stats['CHA'],
        AD=base_stats['AD'],
        FO=base_stats['FO'],
        PV=stats['PV'],
        PM=stats['PM'],
        PRD=stats['PRD'],
        PointDestin=stats['PdD'],
        PO=random.randint(5, 300)
        )
    db.add(carac_data)
    db.commit()
    carac_id = carac_data.id

    character_data = model.PNJ(
        name=name,
        carac_id=carac_id,
        race_id=random.randint(3, 13),
        room_id=room_id
    )

    db.add(character_data)
    db.commit()
    return character_data.id

def generate_base_stats():
    return {
        'COU': random.randint(1, 20),
        'INT': random.randint(1, 20),
        'CHA': random.randint(1, 20),
        'AD': random.randint(1, 20),
        'FO': random.randint(1, 20)
    }

def generate_stats(base_stats):
    stats = {}
    stats['PV'] = base_stats['FO'] * 10
    stats['PM'] = base_stats['CHA'] + (base_stats['INT'] * 9)
    stats['AT'] = base_stats['FO'] + (base_stats['COU'] / 2)
    stats['PRD'] = (base_stats['INT'] * 2) - base_stats['FO'] + base_stats['AD']
    stats['PO'] = 0
    stats['PdD'] = 0
    return stats

def send_monster(monster_name, room_id):
    response = db.query(model.PNJ, model.Carac, model.Race).filter(model.PNJ.name == monster_name, model.PNJ.room_id == room_id)\
                        .filter(model.Carac.id == model.PNJ.carac_id)\
                        .filter(model.Race.id == model.PNJ.race_id)\
                        .all()

    return response

def get_user(username):
    query = db.query(model.Account).filter(model.Account.login == username).all()
    for user in query:
        response = send_user_avatar(user.id)

    return response

def send_user_avatar(user_id):
    response = db.query(model.Avatar, model.Carac, model.Metier, model.Race).filter(model.Avatar.acc_id == user_id)\
                        .filter(model.Carac.id == model.Avatar.carac_id)\
                        .filter(model.Metier.id == model.Avatar.metier_id)\
                        .filter(model.Race.id == model.Avatar.race_id)\
                        .all()

    return response