function logError(txt) {
    console.log("ERROR");
    console.log(JSON.stringify(txt));
}

    var locationws = location.href.replace('http', 'ws').replace(':5000/', ':8765/');
    var ice_serv = {"iceServers": [{"url": "stun:stun.l.google.com:19302"}]};
    //var locationws = 'http://localhost:5000/room/1'.replace('http', 'ws').replace(':5000', ':8765')
    var ws = new WebSocket(locationws);
    var isNegociated = false;

    ws.sync_send = function(txt) {
        if (ws.readyState === 1)
        {
            ws.send(txt);
            return;
        }
        setTimeout(function() {
            ws.sync_send(txt);
        }, 3);
    };
    ////////////
    var pc = new RTCPeerConnection(ice_serv);
    var haveRcvStream = false;

function localDescCreated(desc) {
    console.log("localDescCreated " + desc);
    pc.setLocalDescription(desc, function () {
        console.log("SET LOCAL DESC & SEND");
        txt = JSON.stringify({ "sdp": pc.localDescription });
        console.log("SDP: " + txt)
        console.log("WS: " + ws);
        ws.sync_send(txt);
        console.log("SENDED");
    }, logError);
}

    pc.onaddstream = function(event) {
            var remoteDesc = document.getElementById("remoteVideo");
            console.log("ON ADD STREAM : " + event);
            console.log(JSON.stringify(event));
            pc.addStream(event.stream);
            attachMediaStream(remoteDesc, event.stream);
            var haveRcvStream = true;
    };

    // message de bienvenue
    ws.sync_send(JSON.stringify({'hello': 42}));
    ws.onmessage = function(event) {
        var signal = JSON.parse(event.data);
        console.log("RECU " + event.data)
        if (signal.is_mj) {
            console.log("WE ARE THE SERVER");
            // Add a stream
            var localDesc = document.getElementById("localVideo");
            var constraints = {
                audio: true,
                video: true
            };
            getUserMedia(constraints, function(stream) {
                console.log("GET STREAM TO SEND")
                console.log(stream)
                pc.addStream(stream);
                attachMediaStream(localDesc, stream);
                // je suis le mj, j'envoie l'offre
                pc.createOffer(localDescCreated, logError);
                console.log("CREATE OFFER:" + pc)
            }, logError);
        }
        else if (signal.sdp && signal.sdp.type == "offer")
        {
            console.log("WE RECV OFFER FROM SERVER, CREATE ANSWER FOR GET ONADDSTREAM EVENT");
            pc.setRemoteDescription(new RTCSessionDescription(signal.sdp), function(event) {
                console.log('creating answer...');
                pc.createAnswer(function(answer) {
                    console.log('created answer...');
                    pc.setLocalDescription(answer, function() {
                        console.log('sent answer');
                        ws.send(JSON.stringify(answer));
                    }, logError);
                }, logError);
            }, logError);
        }
        else if (signal.sdp && signal.type == "answer")
        {
            console.log("RECV ANSWER FROM REMOTE CLIENT, REGISTER IT");
            //if (isNegociated == false) {
                pc.setRemoteDescription(new RTCSessionDescription(signal), function() {}, logError);
                isNegociated = true;
            //}
        }
    };