#!/usr/bin/env python3
import asyncio
import websockets
import re
import json

path_assoc = dict()
room_msg = dict()
repath = re.compile('/[^/]+/(\d+)')

def deconnect(ws, path, path_assoc) -> bool:
    print("DECONNEXION %s" % ws)
    path_assoc[path] = None
    del path_assoc[path]
    return True

def wrap(s) -> str:
    if len(s) > 50:
        return s[:50] + '...'
    return s

def dump_room_msg(room_msg):
    txt = ""
    ident = '\t'
    for k1 in sorted(room_msg.keys()):
        txt += str(k1) + " :\n"
        for k2 in sorted(room_msg[k1].keys()):
            txt += ident + wrap(str(k2)) + " :\n"
            for k3 in sorted(room_msg[k1][k2].keys()):
                txt += (ident * 2) + str(k3) + " :\n"
                v = room_msg[k1][k2][k3]
                txt += (ident * 3) + wrap(str(v)) + "\n"
    return txt

class Room:
    def __init__(self, ws):
        self.mj_ws = ws
        self.offer = None
        # map id(ws_client) => answer message
        self.answer = dict()

@asyncio.coroutine
def hello(ws, txtpath):
    global path_assoc
    global repath
    path = repath.search(txtpath).group(1)
    print("RECEIVE {} MSG {}!".format(id(ws), path))
    is_mj = False
    if path not in path_assoc:
        path_assoc[path] = Room(ws)
        is_mj = True
    # Receive Handler
    while True:
        message = yield from ws.recv()
        if message is not None:
            msg = json.loads(message)
            print("JSON<%s>" % repr(msg))
            if 'type' in msg and msg['type'] == 'answer':
                path_assoc[path].answer[id(ws)] = {'answer': message, 'offer_sended': False, 'ws': ws}
                yield from path_assoc[path].mj_ws.send(message)
            elif 'sdp' in msg and msg['sdp']['type'] == 'offer':
                path_assoc[path].offer = message
            elif 'hello' in msg and msg['hello'] == 42:
                if is_mj:
                    print("THIS IS MJ")
                    q = '{"is_mj": true}'
                    yield from ws.send(q)
                else:
                    yield from ws.send(path_assoc[path].offer)
                    # send offer to all client
                    #for client in path_assoc[path].answer.keys():
                    #    if not path_assoc[path].answer[client]['offer_sended']:
                    #        yield from path_assoc[path].answer[client]['ws'].send(path_assoc[path].offer)
                    #        path_assoc[path].answer[client]['offer_sended'] = True
        else:
            print("RECV NONE")

start_server = websockets.serve(hello, 'localhost', 8765)

if __name__ == '__main__':
    print("Running Signalling SERVER!")
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
