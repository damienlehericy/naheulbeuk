from flask import Flask, request
from app.chat import chat
from app.user import user
from app.home import home
from flask import render_template, abort, session
from jinja2 import TemplateNotFound

app = Flask(__name__)

app.register_blueprint(home)
app.register_blueprint(user)
app.register_blueprint(chat)

app.debug = True
app.secret_key = "H@mt@ro"

if __name__ == "__main__":
    # forever serve
    app.run()