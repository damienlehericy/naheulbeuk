from . import user
from flask import render_template, abort, redirect, request, session, escape
from jinja2 import TemplateNotFound
from models import model
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

engine = create_engine('sqlite:///models/naheul.db')
engine.connect()
db = Session(engine)

@user.route('/register', methods=['GET', 'POST'])
def register():
    try:
        options = {
            'error': None,
            'authType': 'register'
        }
        if not 'nickname' in session:
            if request.method == 'POST':
                if request.form['username'] and request.form['email'] and request.form['password']:
                    account = request.form
                    data = model.Account(login=account['username'], email=account['email'], passwd=account['password'])
                    db.add(data)
                    db.commit()
                    return redirect('')
                else:
                    options.error = 'You have not filled out the form.'
        else:
            return redirect('')
        return render_template('user/auth.html', **options)
    except TemplateNotFound:
        abort(404)

@user.route('/logout', methods=['GET'])
def logout():
    try:
        session.clear()
        return redirect('/login')
    except TemplateNotFound:
        abort(404)

@user.route('/login', methods=['GET', 'POST'])
def login():
    try:
        options = {
            'error': None,
            'authType': 'login'
        }
        if 'nickname' not in session:
            if request.method == 'POST':
                if request.form['username'] and request.form['password']:
                    query = db.query(model.Account).filter(
                        model.Account.login == request.form['username'],
                        model.Account.passwd == request.form['password'])
                    response = query.all()
                    for item in response:
                        session['account_id'] = item.id
                        session['nickname'] = item.login
                        session['email'] = item.email
                    return redirect('')
                else:
                    options.error = 'You have not filled out the form.'
        else:
            return redirect('')
        return render_template('user/auth.html', **options)
    except TemplateNotFound:
        abort(404)


@user.route('/create/<int:user_id>/character', methods=['GET', 'POST'])
def create_character(user_id):
    if request.form:
        character = request.form
        carac_data = model.Carac(
            COU=character['COU'],
            INT=character['INT'],
            CHA=character['CHA'],
            AD=character['AD'],
            FO=character['FO'],
            PV=character['PV'],
            PM=character['PM'],
            PRD=character['PRD'],
            PointDestin=character['PdD'],
            PO=character['PO']
            )
        db.add(carac_data)
        db.commit()
        carac_id = carac_data.id

        character_data = model.Avatar(
            name=character['name'],
            carac_id=carac_id,
            metier_id=character['profession'],
            race_id=character['race'],
            acc_id=session['account_id']
        )

        db.add(character_data)
        db.commit()
        session['avatar_id'] = character_data.id

    return redirect('/')

@user.route('/account', methods=['POST'])
def account():
    try:
        # NOTE : Si des personnalisations ou autre pour le joueur, les mettre ici.
        return render_template('user/auth.html', authType='account')
    except TemplateNotFound:
        abort(404)