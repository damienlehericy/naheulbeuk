from . import home
from flask import render_template, abort, redirect, request, session, escape
from jinja2 import TemplateNotFound
from models import model
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
import random
from libs import generator

engine = create_engine('sqlite:///models/naheul.db')
engine.connect()
db = Session(engine)

@home.route("/")
def main():
    try:
        options = {}
        if session and 'account_id' in session:
            if 'avatar_id' not in session:
                query = db.query(model.Avatar).filter(model.Avatar.acc_id == session['account_id'])
                if query.count() < 1:
                    options['avatar_class'] = db.query(model.Metier).all()
                    options['avatar_race'] = db.query(model.Race).all()
                    options['avatar_base_stats'] = generator.generate_base_stats()
                    options['avatar_stats'] = generator.generate_stats(options['avatar_base_stats'])
                else:
                    options['user_avatar'] = generator.send_user_avatar(session['account_id'])
            else:
                query = db.query(model.Avatar).filter(model.Avatar.acc_id == session['account_id'])
                if query.count() < 1:
                    options['avatar_class'] = db.query(model.Metier).all()
                    options['avatar_race'] = db.query(model.Race).all()
                    options['avatar_base_stats'] = generator.generate_base_stats()
                    options['avatar_stats'] = generator.generate_stats(options['avatar_base_stats'])
                else:
                    options['user_avatar'] = generator.send_user_avatar(session['account_id'])

        return render_template('home/home.html', **options)
    except TemplateNotFound:
        abort(404)
