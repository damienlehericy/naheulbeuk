from . import chat
from flask import render_template, abort, session, redirect, request, Response, jsonify, json
from jinja2 import TemplateNotFound
from models import model
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
import re
from libs import generator
import random

engine = create_engine('sqlite:///models/naheul.db')
engine.connect()
db = Session(engine)

@chat.route("/room/gm/<int:room_id>")
def enter_gm(room_id):
    try:
        if not 'nickname' in session:
            return redirect('/login')
        session['MJ'] = True
        options = {'room_id': room_id, 'session': session}
        return render_template('chat/room.html', **options)
    except TemplateNotFound:
        abort(404)

@chat.route("/room/player/<int:room_id>")
def enter_player(room_id):
    try:
        if not 'nickname' in session:
            return redirect('/login')
        session['MJ'] = False
        options = {'room_id': room_id, 'session': session}
        return render_template('chat/room.html', **options)
    except TemplateNotFound:
        abort(404)

@chat.route("/room/message/<int:room_id>/send", methods=['GET', 'POST'])
def send_message(room_id):
    try:
        response = {'status': "OK"}
        message = request.form['content']
        if message == '/roll 20':
            message = generator.launch_dice(20)
            save_message(session['nickname'], room_id, message)
        elif message == '/roll 6':
            message = generator.launch_dice(6)
            save_message(session['nickname'], room_id, message)
        elif re.match('/generate', message):
            if session['MJ']:
                name = message.split(" ")
                generator.generate_monsters(name[1], room_id)
                message = "Une nouvelle créature se ballade, on ne sait pas si il est hostile ou pas !"
                save_message(session['nickname'], room_id, message)
        elif re.match('/monster', message):
            if session['MJ']:
                name = message.split(" ")
                monster = generator.send_monster(name[1], room_id)
                for value in monster:
                    print(value[1].PO)
                    message = "Nom: " + value[0].name \
                              + " </br> COU: " + str(value[1].COU) \
                              + " </br> INT: " + str(value[1].INT)\
                              + " </br> CHA: " + str(value[1].CHA)\
                              + " </br> AD: " + str(value[1].AD) \
                              + " </br> FO: " + str(value[1].FO) \
                              + " </br> PV: " + str(value[1].PV) \
                              + " </br> PM: " + str(value[1].PM) \
                              + " </br> PRD: " + str(value[1].PRD) \
                              + " </br> AT: " + str(value[1].AT) \
                              + " </br> PdD: " + str(value[1].PointDestin) \
                              + " </br> PO: " + str(value[1].PO)

                save_message(session['nickname'], room_id, message)
        else:
            save_message(session['nickname'], room_id, message)
        return jsonify(response)
    except TemplateNotFound:
        abort(404)

def save_message(name, room_id, message):
    data = model.Message(character_name=name, room_id=room_id, message=message)
    db.add(data)
    db.commit()


@chat.route("/room/message/<int:room_id>/get")
def get_message(room_id):
    try:
        query = db.query(model.Message).filter(model.Message.room_id == room_id).order_by(model.Message.id.desc())
        response = []
        for item in query.all():
            json_item = item.tojson()
            response.append(json_item)
        return jsonify(results=response)
    except TemplateNotFound:
        abort(404)

def launch_dice(type):
    dice = 0
    if type == 6:
        dice = random.randint(1, 6)
    else:
        dice = random.randint(1, 20)

    return dice

@chat.route('/wat', methods=['POST'])
def wat():
    try:
        nickname = request.form['nickname']
        query = generator.get_user(nickname)
        for value in query:
            response = {
                'name': value[0].name,
                'COU': value[1].COU,
                'INT': value[1].INT,
                'CHA': value[1].CHA,
                'AD': value[1].AD,
                'FO': value[1].FO,
                'PV': value[1].PV,
                'PM': value[1].PM,
                'AT': value[1].AT,
                'PRD': value[1].PRD,
                'PO': value[1].PO,
                'PdD': value[1].PointDestin,
                'profession': value[2].name,
                'race': value[3].name,
            }
        return jsonify(result=response)
    except TemplateNotFound:
        abort(404)